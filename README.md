API URL = https://murmuring-plateau-77487.herokuapp.com/


If wrong URL is entered, diagHandler will display instructions and so does the frontpage.



To find Country: 
Add /conservation/v1/country/ to the url. Then write country code + &limit(NUMBER) to define limit 
Example: http://localhost:8080/conservation/v1/country/no&limit=5 will display 5 species, species key and year observed in Norway 
To display species, navigate to /conservation/v1/species/ and enter species' key 
Example: http://localhost:8080/conservation/v1/species/2491534 


diagHandler displays API's uptime at the end of the page



I recommend to view the API with Firefox.



Data is gathered from https://www.gbif.org/ (Specie) and https://restcountries.eu/ (Countries)
