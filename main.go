																	// START OF CODE

package main

// IMPORTED LIBRARIES (ONLY NESSESSARY LIBRARIES ARE IMPORTED AS GOLANG WON'T LET YOU IMPORT SOMETHING YOU DON'T USE)

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"encoding/json"
	"bytes"
	"io"
	"time"
)
// FUNCTION THAT DISPLAYS TIME AT API

func init() {
    startTime = time.Now()
}

var startTime time.Time

func uptime() time.Duration {
    return time.Since(startTime)
}

// DEFINING CONST. IN THIS ASSIGNMENT CONSTS ARE USED TO NAVIGATE TO THE URLS

const COUNTRIES  = "https://restcountries.eu/rest/v2/alpha/"
const SPECIES    = "http://api.gbif.org/v1/species/"
const OCCURRENCE = "http://api.gbif.org/v1/occurrence/"

// MAKING STRUCTS

type Country struct {
    Code 	       string `json:"alpha2Code"`
	CountryName    string `json:"name"`
	CountryFlag    string `json:"flag"`
}

type Occurrence struct {
	Results []Result 	  `json:"results"`	
}

type Result struct {
	Species   	   string `json:"species"`
	SpeciesKey 	   int 	  `json:"speciesKey"`
	Year       	   int	  `json:"year"`
}

// COMBINING COUNTRY AND OCCURRENCE STRUCTS

type OcCo struct {
	Occurrence []Result    `json:"results"`
	Country    Country     `json:"country"`
}

type Species struct {
	Key            int 	  `json:"key"`
	Kingdom        string `json:"kingdom"`
	Phylum         string `json:"phylum"`
	Family 		   string `json:"family"`
	Genus 		   string `json:"genus"`
	ScientificName string `json:"scientificName"`	
	CanonicalName  string `json:"canonicalName"`
	BracketYear    int    `json:"bracketYear"`
}

// DIAGHANDLER THAT DISPLAYS GUIDE AND UPTIME OF THE API. SIMPLE FPRINTF MESSEGAES AND UPTIME FROM FUNCTION THAT WAS WRITTEN GLOBALY

func diagHandler(w http.ResponseWriter, r *http.Request) {

	fmt.Fprintf(w, "Find Country: \n")
	fmt.Fprintf(w, "Add /conservation/v1/country/ to the url. Then write country code + &limit(NUMBER) to define limit \n")
	fmt.Fprintf(w, "Example: http://localhost:8080/conservation/v1/country/no&limit=5 will display 5 species, species key and year observed in Norway \n")
	fmt.Fprintf(w, "Display species: \n Navigate to /conservation/v1/species/ and enter species' key \n")
	fmt.Fprintf(w, "Example: http://localhost:8080/conservation/v1/species/2491534")
	fmt.Fprintf(w, "\n\n\n\n API uptime %s\n", uptime())
	
// CHECKING STATUS FOR GBIF.ORG

	resp, err := http.Get("https://www.gbif.org/developer/summary")
    if err != nil {
        log.Fatal(err)
    }

// PRINT THE HTTP STATUS CODE AND STATUS NAME

    fmt.Fprintf(w, "\nStatus for gbif.org: \nHTTP Response Status:", resp.StatusCode, http.StatusText(resp.StatusCode))

    if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
        fmt.Fprintf(w,"HTTP Status is in the 2xx range")
    } else {
        fmt.Fprintf(w, "\nArgh! Broken")
	}
	
// CHECKING STATUS FOR RESTCOUNTRIES.EU

	resp1, err1 := http.Get("https://restcountries.eu/")
    if err1 != nil {
        log.Fatal(err1)
    }

    fmt.Fprintf(w, "\nStatus for restcountries.eu : \nHTTP Response Status:", resp1.StatusCode, http.StatusText(resp1.StatusCode))

    if resp1.StatusCode >= 200 && resp1.StatusCode <= 299 {
        fmt.Fprintf(w, "\nHTTP Status is in the 2xx range")
    } else {
        fmt.Fprint(w, "\nArgh! Broken")
    }
}


// THIS HANDLER IS EXTRA AND WONT BE COMMENTED AS IT WASN'T PART OF THE TASK. SIMPLY DISPLAYS DATA FROM OCCURRENCE DIRECTORY 

func occHandler(w http.ResponseWriter, r *http.Request) {

	parts 		  := strings.Split(r.URL.Path, "/conservation/v1/occurrence/")
	var1 		  := "search?country=" + parts[1]
	url 		  := OCCURRENCE + var1
	response, err := http.Get(url)

	if response.StatusCode >= 400 && response.StatusCode <= 499 {
		fmt.Fprintf(w, "Not found")
	}

    if err != nil {
        fmt.Printf("The HTTP request failed with error %s\n", err)
	}

	var message Occurrence
    err = json.NewDecoder(response.Body).Decode(&message)

    if err != nil {
        log.Print(err)
	}

	var buffer = new(bytes.Buffer)
	enc := json.NewEncoder(buffer)
	enc.Encode(message) 

    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusAccepted)
	io.Copy(w, buffer)
}

// COUNTRYHANDLER

func countryHandler(w http.ResponseWriter, r *http.Request) {

// SPLITTING URLS. PARTS CONTAINS COUNTRYCODE. PARTS1 COMBINES CODE WITH SEARCH?COUNTRY=

	parts  := strings.Split(r.URL.Path, "/conservation/v1/country/")
	parts1 := OCCURRENCE + "search?country=" + parts[1]

// CUTS LAST 2 SIGNS OF COUNTRY, SO &LIMIT=(NUMBER) IS POSSIBLE TO BE ADDED AT THE URL.	

	a := []rune(parts1)
	countryCode := string(a[49:51])
	url    := COUNTRIES + countryCode

// GETTING HTTPS FOR COUNTRIES AND OCCURRENCE

	response,  err  := http.Get(url)
	response1, err1 := http.Get(parts1)

// IN CASE OF ERROR	

	if response.StatusCode >= 400 && response.StatusCode <= 499 {
		fmt.Fprintf(w, "Country code not found")
	}
	if response1.StatusCode >= 400 && response1.StatusCode <= 499 {
		fmt.Fprintf(w, "Not found")
	}

// MAKING 2 VARIABLES; 1 FOR COUNTRY AND 1 FOR OCCURRENCE

	var message1 Country
	var message2 Occurrence
	
// DECODING JSON FROM COUNTRIES AND OCCURRENCE	

	err = json.NewDecoder(response.Body).Decode(&message1)	
	if err != nil {
        panic(err)
	} 

	err1 = json.NewDecoder(response1.Body).Decode(&message2)
	if err1 != nil {
        panic(err1)
	}

// COMBINING DECODED TEXT

	var combined OcCo
	combined.Country    = message1
	combined.Occurrence = message2.Results

// CREATING NEW BUFFER TO ENCODE JSON	

	var buffer = new(bytes.Buffer)

// ENCODING JSON AND FILLING BUFFER WITH COMBINED VARIABLE

	enc := json.NewEncoder(buffer)
	enc.Encode(combined) 

// DISPLAYING RESULTS FOR THE USER

    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusAccepted)
	io.Copy(w, buffer)
	
}

// SIMILAR CODE AS COUNTRYHANDLER

func speciesHandler(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/conservation/v1/species/")			
	query := parts[1]
	url   := SPECIES + query									

	response, err := http.Get(url)													
		if err != nil {																						
			fmt.Println("Bad request %s \n", err)			
		}

		var content Species
		err = json.NewDecoder(response.Body).Decode(&content)

		var buffer = new(bytes.Buffer)
		encode := json.NewEncoder(buffer)
		encode.Encode(content)

		w.Header().Set("content-type", "application/json")
		w.WriteHeader(http.StatusAccepted)
		io.Copy(w, buffer)
}

// MAIN FUNCTION

func main() {

// CHECKS IF PORT IS SET

	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("$PORT must be set")
	}

// HANDLEFUNC THAT WILL RUN WHEN USER ENTERS CORRECT VALUE. AS MENTIONED EARLIR, OCCURRENCEHANDLER IS EXTRA. IT DOES WORK, BUT WONT BE EXPLAINED

	http.HandleFunc("/conservation/v1/country/", countryHandler)
	http.HandleFunc("/conservation/v1/occurrence/", occHandler)
	http.HandleFunc("/conservation/v1/species/", speciesHandler)
	http.HandleFunc("/", diagHandler)

	log.Fatal(http.ListenAndServe(":"+port, nil))
}


																		// END OF CODE